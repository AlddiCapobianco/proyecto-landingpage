// document.querySelector("#login_btn").setAttribute("onclick", "validacion()");
let resultado;
function validacion() {
    let user = document.querySelector("#nombre").value;
    if (user.trim() == "") {
        alert("COMPLETAR NOMBRE Y APELLIDO");
        document.querySelector("#nombre").focus();
        return;
    }

    let email = document.querySelector("#email").value;
    emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

    if (!email.match(emailRegex)) {
        alert("La dirección de email es incorrecta.");
        document.querySelector("#email").focus();
        return;
    }

    let tel = document.querySelector("#tel").value;
    if (tel.trim() == "") {
        alert("COMPLETAR TELEFONO");
        document.querySelector("#tel").focus();
        return;
    }

    let inputResul = document.querySelector("#captcha").value;
    if (inputResul.trim() === "") {
        alert("Tenés que completar la palabra.");
        document.querySelector("#captcha").focus();
        return;
    }
    if (resultado !== inputResul) {
        alert("No es el valor correcto");
        document.querySelector("#captcha").focus();
        return;
    }

    alert("TE SUSCRIBISTE CORRECTAMENTE");
}

let data_array = [];
data_array[0] = { pregunta: "Escribe la palabra:", respuesta: "Taladro" };
data_array[1] = { pregunta: "Escribe la palabra:", respuesta: "Azul" };
data_array[2] = { pregunta: "Escribe la palabra:", respuesta: "Soleado" };
data_array[3] = { pregunta: "Escribe la palabra:", respuesta: "Comida" };
data_array[4] = { pregunta: "Escribe la palabra:", respuesta: "Futuro" };
let indice = Math.floor(Math.random() * 5);
// console.log(indice); - FUNCIONA
// console.log(data_array[indice]); - FUNCIONA

let parrafo = document.querySelector("#consulta");
resultado = data_array[indice].respuesta.trim().toLowerCase();
consulta.innerHTML = `${data_array[indice].pregunta} ${data_array[indice].respuesta}`;





